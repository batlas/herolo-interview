export interface Product {
    id: number,
    name: string,
    city: string,
    street: string,
    phone:string,
    website_url:string,
    image_url:string
}


