import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from './product.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  @Input() product: Product;
  @Output() editProduct = new EventEmitter<Product>();
  constructor() { }

  ngOnInit() {
  }

  onEdit(){
    this.editProduct.emit(this.product);
  }

}
