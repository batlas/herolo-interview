import { Component, OnInit, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { ModalService } from '../_services/modal.service';
import { NgForm } from '@angular/forms';
import { Product } from '../product/product.model';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
    @Input() id: string;

    @Output() editProduct = new EventEmitter<Product>();
    product: any = {};
    private element: any;

    constructor(private modalService: ModalService, private el: ElementRef) {
        this.element = el.nativeElement;
    }

    ngOnInit(): void {
        let modal = this;

        if (!this.id) {
            console.error('modal must have an id');
            return;
        }

        document.body.appendChild(this.element);

        this.element.addEventListener('click', function (e: any) {
            if (e.target.className === 'jw-modal') {
                modal.close();
            }
        });

        this.modalService.add(this);
    }

    ngOnDestroy(): void {
        this.modalService.remove(this.id);
        this.element.remove();
    }

    open(p: Product): void {
        this.element.style.display = 'block';
        document.body.classList.add('jw-modal-open');

        this.product = p;
    }

    close(): void {
        this.element.style.display = 'none';
        document.body.classList.remove('jw-modal-open');
    }


    onSubmit(form: NgForm) {
        this.product.name = form.value.productName;
        this.product.city = form.value.city;
        this.product.phone = form.value.phone;
        this.product.website_url = form.value.website_url;
        this.editProduct.emit(this.product);
        this.close();
    }
}
