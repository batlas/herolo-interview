import { Component, OnInit } from '@angular/core';
import { Product } from './product/product.model';
import { ApiService } from './_services/api.service';
import { ModalService } from './_services/modal.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'web-api-tests';

  products: Product[] = [];

  constructor(private api: ApiService, private modalService: ModalService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.api.getProducts().subscribe(
      (products: any[]) => {
        this.products = products;
      },
      (error) => console.log(error)
    );
  }

  openModal(product: Product) {
    this.modalService.open("custom-modal-1", product);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  editProduct(prod) {
    if(prod && prod.id){
      const index = this.products.findIndex(p=>p.id == prod.id, 0);
      if (index > -1) {
        this.products[index].name = prod.name;
        this.products[index].city = prod.city;
      }
    }
  }
}
