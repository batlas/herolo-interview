import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Product } from '../product/product.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: Http) { }


  getProducts() {
    return this.http.get('https://api.openbrewerydb.org/breweries?by_state=new_york').pipe(
      map((response: Response) => {
        let data = response.json();
        let products: Product[] = [];

        data.forEach((item, index) => {
          const image_url = "https://loremflickr.com/320/240/?random=" + (index+1);
          const product:Product = {
            'id': item.id,
            'name': item.name,
            'city': item.city,
            'street': item.street,
            'phone': item.phone,
            'website_url': item.website_url,
            'image_url': image_url
          }

          products.push(product);
        });

        return products;
      })
    );
  }
}
